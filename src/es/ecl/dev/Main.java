package es.ecl.dev;

public class Main {

	public static void main(String[] args) {
		Main program = new Main();
		program.start();
	}
	
	public void start() {
		Message m = new Message("Hello world!");
		System.out.println(m.toString());
	}

}
