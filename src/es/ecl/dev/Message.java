package es.ecl.dev;

public class Message {
	
	private String mes;
	
	public Message(String mes) {
		this.mes=mes;
	}
	
	public String toString() {
		return this.mes;
	}

}
